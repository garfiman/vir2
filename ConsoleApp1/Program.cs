﻿using System;
using RestSharp;

namespace VIR2
{
    public static class Program
    {
        public static void Main()
        {
            Console.WriteLine("Input URL:");
            var baseUrl = Console.ReadLine();
            IRestClient client = new RestClient();
            try
            {
               client = new RestClient(baseUrl);
            } catch (Exception)
            {
               
            }

            Console.WriteLine("Input method: get / post");
            string inputMethodType = String.Empty;

            while(inputMethodType != "get" || inputMethodType != "post")
            {
                inputMethodType = Console.ReadLine();
                if (inputMethodType == "get" || inputMethodType == "post")
                    break;
                Console.WriteLine("Non-valid input method");
            }

            RestRequest request = new RestRequest();

            if (inputMethodType == "get")
            {
                request = new RestRequest(baseUrl, Method.GET);
            }

            if (inputMethodType == "post")
            {
                request = new RestRequest(baseUrl, Method.POST);
            }

            var menuInput = String.Empty;

            while (menuInput != "0")
            {
                Console.WriteLine("Type 0 to execute request");
                Console.WriteLine("Type 1 to add headers");
                Console.WriteLine("Type 2 to add parameters");
                menuInput = Console.ReadLine();
                if (menuInput == "1")
                {
                    Console.WriteLine("Type header name");
                    string headerName = Console.ReadLine();
                    Console.WriteLine("Type header value");
                    string headerValue = Console.ReadLine();
                    request.AddHeader(headerName, headerValue);
                    Console.WriteLine("Header added: name: " + headerName + ", value: " + headerValue);

                }
                else
                if (menuInput == "2")
                {
                    Console.WriteLine("Type parameter name");
                    string parameterName = Console.ReadLine();
                    Console.WriteLine("Type parameter value");
                    string parameterValue = Console.ReadLine();
                    request.AddParameter(parameterName, parameterValue);
                    Console.WriteLine("Parameter added: name: " + parameterName + ", value: " + parameterValue);

                } 
                else
                if (menuInput == "0")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Non-valid input");
                    Console.WriteLine("--------");
                }
            }

            var resp = client.Execute(request);
            Console.WriteLine("-------------------------");
            Console.Write(resp.Content);
            Console.WriteLine("-------------------------");
            return;
        }
    }
}